from random import randint, choice

import gym
import matplotlib.pyplot as plt
import numpy as np

from spe_ed_game.src.game.directions import Direction
from spe_ed_game.src.game.moves import Move
from spe_ed_game.src.game.player import Player
from spe_ed_game.src.game.spe_ed_game import Game

KERNEL_WIDTH = 31
KERNEL_HEIGHT = 31
GAME_WIDTH = 50
GAME_HEIGHT = 50


class SpeedEnv(gym.Env):
    metadata = {'render.modes': ['human']}
    game: Game

    FPS = 100  # Frames per second.
    block_size = 20

    def __init__(self, opponents):
        super(SpeedEnv, self).__init__()
        self.agents_num = len(opponents)
        # agent can make 5 different actions
        self.action_space = gym.spaces.Discrete(5)
        # observation space for the agent
        self.observation_space = gym.spaces.Box(low=0, high=1, shape=(1, KERNEL_WIDTH * KERNEL_HEIGHT + 2),
                                                dtype=np.uint8)
        # load opponents and reset env
        self.reward_range = (-1, 6)
        self.opponents = opponents
        self.opponents_num = len(opponents)
        self.previous_alive_agents = self.opponents_num
        self.alive_agents = self.opponents_num
        self.reset()

    def step(self, player_action):
        # step the environment -> make move and return observation, reward and information
        # make move for opponents
        for p_num in range(0, self.opponents_num):
            action, _ = self.opponents[p_num].predict(self._getObservation(p_num + 1))
            for i, move in enumerate(Move):
                if i == action:
                    action = move
                    break
            self.game.makeMove(p_num + 1, action)
        # make own move
        for i, move in enumerate(Move):
            if i == player_action:
                player_action = move
                break
        self.game.makeMove(0, player_action)
        self.game.update()

        self.alive_agents = self._getAlivePlayerCount()
        # get observation, reward and info
        obs = self._getObservation(0)
        reward = self._getReward()
        info = {'alive': self.game.players[0].alive,
                'jump': self.game.players[0].round % 6 == 0}
        done = not self.game.players[0].alive
        self.previous_alive_agents = self.alive_agents
        return obs, reward, done, info

    def reset(self):
        # resets the environment to the initial state
        Player.count = 1
        players = []
        for _ in range(0, self.opponents_num + 1):
            players.append(Player((randint(0, GAME_WIDTH - 1),
                                   randint(0, GAME_HEIGHT - 1)),
                                  choice(list(Direction)),
                                  [randint(0, 255), randint(0, 255), randint(0, 255)],
                                  None))
        self.game = Game(GAME_WIDTH, GAME_HEIGHT, players, self.FPS, False, self.block_size)
        # returns inital observation
        return self._getObservation(0)

    def render(self, mode='human'):
        # renders the game with matplotlib.pyplot
        field = []
        for yCell in range(GAME_HEIGHT):
            line = []
            for xCell in range(GAME_WIDTH):
                line.append(self.game.field[yCell][xCell].player_num)
            field.append(line)
        plt.clf()
        plt.imshow(field)
        plt.pause(0.000001)

    def close(self):
        # closes the game
        self.game.exit()
        del self.game

    def _getObservation(self, p_num: int) -> [[int]]:
        # crops the board to a given size around the head of the player
        x, y = self.game.players[p_num].position
        values = []
        for row, y_offset in enumerate(range(-15, 16), 0):
            values.append([])
            for x_offset in range(-15, 16):
                try:
                    if y + y_offset >= 0 and x + x_offset >= 0:
                        values[row].append(0 if self.game.field[y + y_offset][x + x_offset].player_num == 0 else 1)
                    else:
                        raise IndexError
                except IndexError:
                    values[row].append(1)
        values = np.array(values)
        # rotates the crop according to the player direction
        dir = self.game.players[p_num].direction.value
        ORIENTATIONS = ["up", "right", "down", "left"]
        for index, value in enumerate(ORIENTATIONS):
            if value == dir:
                for _ in range(index):
                    values = np.rot90(values)
        obs = values.flatten().tolist()
        # add speed and jump info to the observation
        obs.append(self.game.players[0].round % 6 == 0)
        obs.append(self.game.players[0].speed)
        obs = np.array([obs])
        return obs

    def _getReward(self) -> int:
        # reward -> (o = alive, -1 = dead)
        if not self.game.players[0].alive:
            return -1
        return 0

    def _getAlivePlayerCount(self) -> int:
        # get count of living players
        alive_player_count = 0
        for player in self.game.players:
            if player.alive:
                alive_player_count += 1
        return alive_player_count

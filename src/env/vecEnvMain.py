# Experimental vector environment Wrapper for the speed_env
import datetime

from stable_baselines import PPO2
from stable_baselines.common import make_vec_env
from stable_baselines.common.policies import MlpPolicy

from src.env.speed_env import SpeedEnv


if __name__ == '__main__':
    num_agents = 6

    placeholder_env = SpeedEnv([])
    models = [PPO2(MlpPolicy, placeholder_env, verbose=1) for _ in range(num_agents)]
    placeholder_env.close()

    timestamp = str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))

    for retrain_round in range(20):
        for model_num, model in enumerate(models):
            print("Round: "+str(retrain_round)+", Modelnumber: "+str(model_num))
            env = make_vec_env(SpeedEnv, env_kwargs={"opponents": [m for m in models if m is not model]}, n_envs=4)
            model.set_env(env)
            model.learn(total_timesteps=100000)
            path = timestamp+"ppo1-snake"+str(retrain_round)+"-"+str(model_num)
            model.save(path)
            env.close()

    model = models[0]
    model.save(timestamp+"ppo1-snake")
    env = SpeedEnv(opponents=[m for m in models if m is not model])
    obs = env.reset()
    for _ in range(10000):
        action, _ = model.predict(obs)
        obs, _, _, _ = env.step(action)
        env.render()

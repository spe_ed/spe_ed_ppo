import datetime

from stable_baselines import PPO1
from stable_baselines.common.policies import MlpPolicy

from src.env.speed_env import SpeedEnv

if __name__ == '__main__':
    num_agents = 6
    # creates 6 new networks
    placeholder_env = SpeedEnv([])
    models = [PPO1(MlpPolicy, placeholder_env, verbose=1) for _ in range(num_agents)]
    placeholder_env.close()
    # loads generation of networks
    # models = [PPO1.load(("20201217-160126ppo1-snake0-" + str(x))) for x in range(6)]
    # loads one network 6 times
    # models = [PPO1.load(("20201217-181540ppo1-snake0-4")) for _ in range(6)]
    # timestamp when the training starts
    timestamp = str(datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))
    for retrain_round in range(20):
        # retrains the models a given amount of times
        for model_num, model in enumerate(models):
            print("Round: " + str(retrain_round) + ", Modelnumber: " + str(model_num))
            # create a new env with one player as trainee and the other ones as opponents
            env = SpeedEnv(opponents=[m for m in models if m is not model])
            # trains network
            model.set_env(env)
            model.learn(total_timesteps=100000)
            path = timestamp + "ppo1-snake" + str(retrain_round) + "-" + str(model_num)
            # saves network
            model.save(path)
            env.close()

    model = models[0]
    # saves last network and starts a game to show result
    model.save(timestamp + "ppo1-snake")
    env = SpeedEnv(opponents=[m for m in models if m is not model])
    obs = env.reset()
    while env.game.running():
        # game loop
        print(env.game.running())
        action, _ = model.predict(obs)
        print(action)
        obs, _, _, _ = env.step(action)
        print("RENDER")
        env.render()

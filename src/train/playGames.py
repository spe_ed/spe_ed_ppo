import time

from stable_baselines import PPO1
from src.env.speed_env import SpeedEnv

if __name__ == '__main__':
    # loads generation of networks
    models = [PPO1.load(("20201217-195658ppo1-snake1-"+str(x))) for x in range(6)]
    # loads one network 6 times
    #models = [PPO1.load("20201217-195658ppo1-snake1-5") for x in range(6)]
    model = models[0]
    # creates new environment
    env = SpeedEnv(opponents=[m for m in models if m is not model])
    obs = env.reset()
    while env.game.running():
        # game loop
        print(env.game.running())
        action, _ = model.predict(obs)
        print(action)
        obs, _, _, _ = env.step(action)
        print("RENDER")
        env.render()
import json
import os
import sys

import websockets
from stable_baselines import PPO1
import numpy as np
import matplotlib.pyplot as plt

from spe_ed_game.src.game.moves import Move


class WebSocketClient():

    def __init__(self):
        # loads network
        if len(sys.argv) != 2:
            print("Wrong parameters!")
            sys.exit(-1)
        try:
            self.net = PPO1.load("networks/" + sys.argv[1])
        except:
            print("Wrong parameters!")
            sys.exit(-1)

        self.round = 1

    def getObservation(self, unparsed_message, round) -> [[int]]:
        message = json.loads(unparsed_message)
        board = message["cells"]
        # displays game when DISPLAY_GAME is true
        if os.getenv("DISPLAY_GAME") == "true":
            plt.clf()
            plt.imshow(board)
            plt.pause(0.000001)
        x = message["players"][str(message["you"])]["x"]
        y = message["players"][str(message["you"])]["y"]
        # crops the board to a given size around the head of the player
        values = []
        for row, y_offset in enumerate(range(-15, 16), 0):
            values.append([])
            for x_offset in range(-15, 16):
                try:
                    if y + y_offset >= 0 and x + x_offset >= 0:
                        values[row].append(0 if board[y + y_offset][x + x_offset] == 0 else 1)
                    else:
                        raise IndexError
                except IndexError:
                    values[row].append(1)
        values = np.array(values)
        # rotates the crop according to the player direction
        dir = message["players"][str(message["you"])]["direction"]
        ORIENTATIONS = ["up", "right", "down", "left"]
        for index, value in enumerate(ORIENTATIONS):
            if value == dir:
                for _ in range(index):
                    values = np.rot90(values)
        obs = values.flatten().tolist()
        # add speed and jump info to the observation
        obs.append(round % 6 == 0)
        obs.append(message["players"][str(message["you"])]["speed"])
        obs = np.array([obs])
        return obs

    async def connect(self):
        '''
            Connecting to webSocket server

            websockets.client.connect returns a WebSocketClientProtocol, which is used to send and receive messages
        '''
        uri = os.environ['URL'] + "?key=" + os.environ['KEY']
        self.connection = await websockets.client.connect(uri)
        if self.connection.open:
            print('Connection stablished. Client correcly connected')
            return self.connection

    async def sendMessage(self, message):
        '''
            Sending message to webSocket server
        '''
        await self.connection.send(message)

    async def receiveMessage(self, connection):
        '''
            Receiving all server messages and handling them
        '''
        while True:
            try:
                # get game state from server
                message = await connection.recv()
                print("GameState received")
                # calculate best move
                move_number, _ = self.net.predict(self.getObservation(message, self.round))
                for index, move in enumerate(Move):
                    if move_number == index:
                        best_move = move
                        break
                # print and send best move to server
                print("Picking " + best_move.value)
                await self.sendMessage(json.dumps({"action": best_move.value}))
                self.round += 1
            except websockets.exceptions.ConnectionClosed:
                print('Connection with server closed')
                break

FROM python:3.6
COPY . .
ENV URL "ws://host.docker.internal:5000"
ENV KEY "keykey"
ENV NETWORK "20201219-203721ppo1-snake2-5"
RUN apt update
RUN apt -y install cmake libopenmpi-dev python3-dev zlib1g-dev python3-opencv
RUN pip3 install -r requirements.txt
EXPOSE 5000
CMD python3.6 -m src.main $NETWORK